## bm 
| 字段名 | 类型 | 空 | 默认值 | 说明 |
| :----  | :---- | :---- | :---- | :---- |
| bmid | int(11) |  |  | 部门id |
| bmmc | varchar(48) | 可 | NULL | 部门名称 |
| bmfzr | int(11) | 可 | NULL | 部门负责人 |

## ry 
| 字段名 | 类型 | 空 | 默认值 | 说明 |
| :----  | :---- | :---- | :---- | :---- |
| ryid | int(11) |  |  | 人员id |
| xm | varchar(50) |  |  | 姓名 |
| sr | date | 可 | NULL | 生日 |
| gz | decimal(8,2) |  |  | 工资 |
| sg | int(11) | 可 | NULL | 身高 |
| bm | int(11) | 可 | NULL | 部门 |

