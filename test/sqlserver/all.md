## rtq_szse_quotation 
| 字段名 | 类型 | 长度 | 空 | 默认值 | 说明 |
| :----  | :---- | ----: | :---- | :---- | :---- |
| _date | date | 3 | 可 |  |  |
| code | varchar | 12 |  |  |  |
| source | varchar | 64 | 可 |  |  |
| p_close | decimal | 9 | 可 |  |  |
| _open | decimal | 9 | 可 |  |  |
| _high | decimal | 9 | 可 |  |  |
| _low | decimal | 9 | 可 |  |  |
| _close | decimal | 9 | 可 |  |  |
| buyPrice1 | decimal | 9 | 可 |  |  |
| buyVolume1 | int | 4 | 可 |  |  |
| sellPrice1 | decimal | 9 | 可 |  |  |
| sellVolume1 | int | 4 | 可 |  |  |
| buyPrice2 | decimal | 9 | 可 |  |  |
| buyVolume2 | int | 4 | 可 |  |  |
| sellPrice2 | decimal | 9 | 可 |  |  |
| sellVolume2 | int | 4 | 可 |  |  |
| buyPrice3 | decimal | 9 | 可 |  |  |
| buyVolume3 | int | 4 | 可 |  |  |
| sellPrice3 | decimal | 9 | 可 |  |  |
| sellVolume3 | int | 4 | 可 |  |  |
| buyPrice4 | decimal | 9 | 可 |  |  |
| buyVolume4 | int | 4 | 可 |  |  |
| sellPrice4 | decimal | 9 | 可 |  |  |
| sellVolume4 | int | 4 | 可 |  |  |
| buyPrice5 | decimal | 9 | 可 |  |  |
| buyVolume5 | int | 4 | 可 |  |  |
| sellPrice5 | decimal | 9 | 可 |  |  |
| sellVolume5 | int | 4 | 可 |  |  |
| _amount | bigint | 8 | 可 |  |  |
| _money | decimal | 9 | 可 |  |  |
| peRatio | decimal | 9 | 可 |  |  |
| halfamount | bigint | 8 | 可 |  |  |
| halfmoney | decimal | 9 | 可 |  |  |
| date_created | datetime | 8 | 可 |  |  |

## bm 
| 字段名 | 类型 | 长度 | 空 | 默认值 | 说明 |
| :----  | :---- | ----: | :---- | :---- | :---- |
| bmid | numeric | 9 |  |  |  |
| bmmc | varchar | 48 | 可 |  |  |
| bmfzr | int | 4 | 可 |  |  |

## ry 
| 字段名 | 类型 | 长度 | 空 | 默认值 | 说明 |
| :----  | :---- | ----: | :---- | :---- | :---- |
| ryid | numeric | 9 |  |  |  |
| xm | varchar | 50 |  |  |  |
| sr | datetime | 8 | 可 |  |  |
| gz | decimal | 5 |  |  |  |
| sg | int | 4 | 可 |  |  |
| bm | int | 4 | 可 |  |  |

