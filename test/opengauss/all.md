## ry 人员
| 字段名 | 类型 | 长度 | 空 | 默认值 | 说明 |
| :----  | :---- | ----: | :---- | :---- | :---- |
| ryid | int4 | 4 |  | nextval('dbt.ry_ryid_seq'::regclass) | 人员id |
| xm | varchar | 50 |  |  | 姓名 |
| sr | timestamp | 8 |  |  | 生日 |
| gz | numeric | 8 |  |  | 工资 |
| sg | int4 | 4 |  |  | 身高 |
| bm | int4 | 4 |  |  | 部门 |

## bm 部门
| 字段名 | 类型 | 长度 | 空 | 默认值 | 说明 |
| :----  | :---- | ----: | :---- | :---- | :---- |
| bmid | int4 | 4 |  | nextval('dbt.bm_bmid_seq'::regclass) | 部门id |
| bmmc | varchar | 48 |  |  | 部门名称 |
| bmfzr | int4 | 4 |  |  | 部门负责人 |

