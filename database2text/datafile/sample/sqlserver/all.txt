code=gbk

:connect
driver=mssql
#dbinfo={"server":"服务器地址","database":"库名","user":"用户名","password":"密码","port":1433,"readonly":true}
dbcfg=dbt_sqlserver

:readdata

#:export
#export段设置导出数据库里的相应内容为文本文件
#datadir=test/sqlserver/export

:render

file=test/sqlserver/all.md
help=n
#使用help=y可以打印出传入模板的数据，方便写模板的时候使用
#table=sw_program sw t_server
start=## {{ tname }} 
## {{ tname }} {{ tdesc }}
| 字段名 | 类型 | 长度 | 空 | 默认值 | 说明 |
| :----  | :---- | ----: | :---- | :---- | :---- |
{% for c in md %}| {{ c.name }} | {{ c.type }} | {{ c.size }} | {% if c.null %}可{% endif %} | {{ c.default or "" }} | {{ c.desc }} |
{% endfor %}
end=

#根据模板生成dot文件，调用neato生成pdf文件
:dot
dotdir=test/sqlserver
pdfdir=local/sqlserver
filename=all
dotcmd=neato

:end
